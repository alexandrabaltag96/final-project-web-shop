Proiect final web-shop
O scurtă prezentare a proiectului meu.
Acesta este testul final pentru Alexandra, în cadrul cursului FastTrackIt Test Automation.
Inginer software: Baltag Dana - Alexandra
Scopul proiectului:
Implementarea de teste care au ca scop testarea unor funcționalități de bază ale aplicației, cum ar fi: Produse, Coș, Lista de dorințe, Mesaj de salut.

Descriere:
DemoShop este un magazin demonstrativ de testare. Pe interfața web am selectat produse pe categorii, am testat introducerea produselor în cos, wishlist. Am verificat, de asemenea, unele produse și logo-ul paginii de pornire, butonul pentru coș, mesajul de salut, fucționalitatea butonului de autentificare,verificarea butonului de favorite,verificarea produselor în modul de sortare etc.

Pagină web pentru test:
https://fasttrackit-test.netlify.app/

Stiva tehnică utilizată:
Java 17
Ideea IntelliJ
Apache Maven
Cadrul Selenide
Modelul PageObjects
Test NG
Gitlab
browser Google Chrome

Test implementat:
Test Name	Execution Status	Date
1. Verify homePage Logo/ Verificați sigla paginii de pornire	PASSED	07.06.2022
2. Verify shopping cart button is visible/ Verificați că butonul Coș de cumpărături este vizibil	PASSED	07.06.2022
3. Verify home cart redirects where it should/ Verificați redirecționările coșului de acasă unde ar trebui	PASSED	07.06.2022
4. Verify greetings message is Hello guest/ Verificați că mesajul de salut este Salut	PASSED	07.06.2022
5. Verify 3rd element is Awesome Metal Chair/ Verificați că al treilea element este Awesome Metal Chair	PASSED	07.06.2022
6. Verify Element Href Is Correctly Formatted/ Verificați că elementul Href este formatat corect	PASSED	07.06.2022
7. Check question button opens help modal/ Verificați, butonul de întrebare dacă deschide modulul de ajutor	PASSED	07.06.2022
8. Verify sort mode products/ Verificați produsele în modul de sortare	PASSED	07.06.2022
9. Check favorites button changes icon/ Verificați, butonul de favorite dacă modifică pictograma	PASSED	07.06.2022
10. Verify favorite button adds to NoNfavorites page/ Verificați dacă butonul favorit se adaugă la pagina de preferințe NoN	PASSED	07.06.2022
11. Add to non favorites/ Adaugați la non-favorite	PASSED	07.06.2022
12. Verify normal functionality of the button login/ Verificați funcționalitatea normală a butonului de conectare	PASSED	07.06.2022
13. Cart icon updates upon adding a product to cart/ Pictograma coș se actualizează la adăugarea unui produs în coș	PASSED	07.06.2022
14. Add Element To Cart By Index/ Adăugați elementul în coș după index	PASSED	07.06.2022
testele sunt executate folosind Test NG Launch Configs
